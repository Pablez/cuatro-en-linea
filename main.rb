#Solución al problema "Cuatro en línea" de Aurorajobs
#Realizado por Pablo García García (22/09/2022)

#Creamos la clase Tablero, atributos y métodos
class Tablero
    attr_accessor :row, :col, :default_value, :matriz
    def initialize()
        self.row = 6
        self.col = 7
        self.default_value = 0
        self.matriz = Array.new(row).map{Array.new(col,default_value)}
    end

    #Recorre la matriz mostrando los valores
    def mostrar()
        puts "\r"
        self.matriz.each do |numero|
            print "\t\t\t\t", numero, "\n"
        end
        puts "\r"
    end

    #Comprobamos que la casilla esté dentro de los límites y que no esté ocupada. Rellenamos.
    def comprobarCasilla(columna, jugador)
        fila = self.row - 1
        if columna <= 6 && columna >= 0
            while self.matriz[fila][columna] == 1 || self.matriz[fila][columna] == 2
                fila -= 1
                if fila < 0
                    return false
                end
            end
            self.matriz[fila][columna]=jugador
            return true
        else
            return false
        end
    end

    #Pedimos que introduzca número de columna
    def turno(jugador)
        columna = gets.to_i - 1
        if comprobarCasilla(columna, jugador)
        else
            loop do
                print "Columna incorrecta, introduce de nuevo: "
                columna = gets.to_i - 1
                if comprobarCasilla(columna, jugador)
                    break
                end
            end
        end
    end

    #Comprobamos que haya 4 casillas en línea en cualquiera de las direcciones
    def comprobarVictoria(jugador)
        cuatro = false
        #comprobar vertical
        for i in 0..2 do
            for j in 0..6 do
                if self.matriz[i][j] == jugador && self.matriz[i][j] == self.matriz[i+1][j]  && self.matriz[i][j] == self.matriz[i+2][j]  && self.matriz[i][j] == self.matriz[i+3][j]
                    cuatro = true
                    break
                end
            end
        end

        #comprobar horizontal
        for i in 0..5 do
            for j in 0..3 do
                if self.matriz[i][j] == jugador && self.matriz[i][j] == self.matriz[i][j+1]  && self.matriz[i][j] == self.matriz[i][j+2]  && self.matriz[i][j] == self.matriz[i][j+3]
                    cuatro = true
                    break
                end
            end
        end


        #comprobar diagonal derecha abajo
        for i in 0..2 do
            for j in 0..3 do
                if self.matriz[i][j] == jugador && self.matriz[i][j] == self.matriz[i+1][j+1]  && self.matriz[i][j] == self.matriz[i+2][j+2]  && self.matriz[i][j] == self.matriz[i+3][j+3]
                    cuatro = true
                    break
                end
            end
        end

        #comprobar diagonal izquierda abajo
        for i in 0..2 do
            for j in 3..6 do
                if self.matriz[i][j] == jugador && self.matriz[i][j] == self.matriz[i+1][j-1]  && self.matriz[i][j] == self.matriz[i+2][j-2]  && self.matriz[i][j] == self.matriz[i+3][j-3]
                    cuatro = true
                    break
                end
            end
        end
        if cuatro
            return true
        end
    end
end

#Creamos la clase Jugador
class Jugador
    attr_accessor :nombre, :fichas, :ganador, :aciertos
    def initialize(nombre)
        self.nombre = nombre
        self.fichas = 21
        self.ganador = false
    end

    def mensajeVictoria(nombre)
        print "Victoria!!!!\nGanador: #{nombre}"
    end
end


#Creamos las instancias de los jugadores con el nombre que se introduzca, y la instancia del tablero
puts "Introduce nombre del Player One: "
nombre = gets
playerOne = Jugador.new(nombre)
puts "Bienvenid@ #{playerOne.nombre}"
puts "Introduce nombre del Player Two: "
nombre = gets
playerTwo = Jugador.new(nombre)
puts "Bienvenid@ #{playerTwo.nombre}"

tablero = Tablero.new()
tablero.mostrar()


#Comenzamos los turnos
while (playerOne.ganador == false || playerTwo.ganador == false) && playerTwo.fichas != 0

    if playerTwo.fichas == 0
        print "Empate!!! Partida terminada sin ganador"
        break
    end

    print "Turno de ", playerOne.nombre, "Introduce número de columna (1-7): "
    tablero.turno(1)
    playerOne.fichas -= 1
    tablero.mostrar()
    if tablero.comprobarVictoria(1)
        playerOne.ganador = true
        playerOne.mensajeVictoria(playerOne.nombre)
        break
    end

    print "Turno de ", playerTwo.nombre, "Introduce número de columna (1-7): "
    tablero.turno(2)
    playerTwo.fichas -= 1
    tablero.mostrar()
    if tablero.comprobarVictoria(2)
        playerTwo.ganador = true
        playerTwo.mensajeVictoria(playerTwo.nombre)
        break
    end
end

